<?php namespace Xmgravity\Blog\Controllers\Api\V1;
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2013, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Input;
use Lang;
use Platform\Routing\Controllers\ApiController;
use Response;
use Str;
use Validator;

class BlogController extends ApiController {

	/**
	 * Holds the form validation rules.
	 *
	 * @var array
	 */
	protected $validationRules = array(
		'name'    => 'required',
		'slug'    => 'required|unique:blog',
		'value'   => 'required',
		'enabled' => 'required',
	);

	/**
	 * Holds the blog model.
	 *
	 * @var Xmgravity\Blog\Models\Blog
	 */
	protected $model;

	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->model = app('Xmgravity\Blog\Models\Blog');
	}

	/**
	 * Display a listing of blog using the given filters.
	 *
	 * @return Cartalyst\Api\Http\Response
	 */
	public function index()
	{
		if ($limit = Input::get('limit'))
		{
			$blog = $this->model->paginate($limit);
		}
		else
		{
			$blog = $this->model->all();
		}

		return Response::api(compact('blog'));
	}

	/**
	 * Create blog.
	 *
	 * @return Cartalyst\Api\Http\Response
	 */
	public function create()
	{
		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $this->validationRules);

		// If validation fails, we'll exit the operation now
		if ($validator->fails())
		{
			return Response::api(array('errors' => $validator->errors()), 422);
		}

		//
		$blog = new $this->model;

		//
		$blog->name    = $name = Input::get('name');
		$blog->slug    = Str::slug($name);
		$blog->enabled = Input::get('enabled');
		$blog->value   = Input::get('value');

		// Was the blog created?
		if ($blog->save())
		{
			// Blog successfully created
			return Response::api(compact('blog'));
		}

		// There was a problem creating the blog
		return Response::api(Lang::get('xmgravity/blog::message.error.create'), 500);
	}

	/**
	 * Returns information about the given blog.
	 *
	 * @param  mixed  $id
	 * @return Cartalyst\Api\Http\Response
	 */
	public function show($id = null)
	{
		// Do we only want the enabled blog?
		if (Input::get('enabled'))
		{
			$this->model->where('enabled', 1);
		}

		// Try slug first
		if ( ! $blog = $this->model->where('slug', $id)->first() and ! $blog = $this->model->where('id', $id)->first())
		{
			return Response::api(Lang::get('xmgravity/blog::message.blog_not_found', compact('id')), 404);
		}

		return Response::api(compact('blog'));
	}

	/**
	 * Updates the given blog.
	 *
	 * @param  mixed  $id
	 * @return Cartalyst\Api\Http\Response
	 */
	public function update($id = null)
	{
		// Do we have the blog slug?
		if ( ! is_numeric($id))
		{
			$blog = $this->model->where('slug', $id)->first();
		}

		// We must have the blog id
		else
		{
			$blog = $this->model->where('id', $id)->first();
		}

		// Check if the blog exists
		if (is_null($blog))
		{
			return Response::api(Lang::get('xmgravity/blog::message.blog_not_found', compact('id')), 404);
		}

		// Get all the inputs
		$input = Input::all();

		// Update the validation rules, so it ignores the current blog slug.
		$this->validationRules['slug'] = "required|unique:blog,slug,{$blog->slug},slug";

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make($input, $this->validationRules);

		// If validation fails, we'll exit the operation now
		if ($validator->fails())
		{
			return Response::api(array('errors' => $validator->errors()), 422);
		}

		//
		$blog->name    = $name = Input::get('name');
		$blog->slug    = Str::slug($name);
		$blog->enabled = Input::get('enabled');
		$blog->type    = $type = Input::get('type');

		// Database
		if ($type === 'database')
		{
			$blog->value = Input::get('value');
			$blog->file  = null;
		}

		// Filesystem
		else
		{
			$blog->value = null;
			$blog->file = Input::get('file');
		}

		// Was the blog updated?
		if ($blog->save())
		{
			return Response::api(compact('blog'));
		}

		// There was a problem updating the blog
		return Response::api(Lang::get('xmgravity/blog::message.error.update'), 500);
	}

	/**
	 * Deletes the given blog.
	 *
	 * @param  mixed  $id
	 * @return Cartalyst\Api\Http\Response
	 */
	public function destroy($id = null)
	{
		// Do we have the blog slug?
		if ( ! is_numeric($id))
		{
			$blog = $this->model->where('slug', $id)->first();
		}

		// We must have the blog id
		else
		{
			$blog = $this->model->where('id', $id)->first();
		}

		// Check if the blog exists
		if (is_null($blog))
		{
			return Response::api(Lang::get('xmgravity/blog::message.blog_not_found', compact('id')), 404);
		}

		// Delete the blog
		$blog->delete();

		// Blog successfully deleted
		return Response::api(Lang::get('xmgravity/blog::message.success.delete'), 204);
	}

}
