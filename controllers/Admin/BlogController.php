<?php namespace Xmgravity\Blog\Controllers\Admin;
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2013, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use API;
use Cartalyst\Api\Http\ApiHttpException;
use Config;
use DataGrid;
use Illuminate\Support\MessageBag as Bag;
use Input;
use Lang;
use Platform\Admin\Controllers\Admin\AdminController;
use Xmgravity\Blog\Models\Blog;
use Redirect;
use Symfony\Component\Finder\Finder;
use View;

class BlogController extends AdminController {

	/**
	 * Blog management main page.
	 *
	 * @return mixed
	 */
	public function getIndex()
	{
		// Set the current active menu
		set_active_menu('admin-xmgravity-blog');

		// Show the page
		return View::make('xmgravity/blog::index');

	}

	/**
	 * Datasource for the blog Data Grid.
	 *
	 * @return Cartalyst\DataGrid\DataGrid
	 */
	public function getGrid()
	{
		// Get the blog list
		$response = API::get('v1/blog');

		//
		return DataGrid::make($response['blog'], array(
			'id',
			'name',
			'slug',
			'enabled',
			'created_at',
		));
	}

	/**
	 * Create new blog.
	 *
	 * @return View
	 */
	public function getCreate()
	{
		return $this->showForm(null, 'create');
	}

	/**
	 * Create new blog form processing.
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
		return $this->processForm();
	}

	/**
	 * Blog update.
	 *
	 * @param  mixed  $id
	 * @return mixed
	 */
	public function getEdit($id = null)
	{
		return $this->showForm($id, 'update');
	}

	/**
	 * Blog update form processing.
	 *
	 * @param  mixed  $id
	 * @return Redirect
	 */
	public function postEdit($id = null)
	{
		return $this->processForm($id);
	}

	/**
	 * Blog copy.
	 *
	 * @param  mixed  $id
	 * @return mixed
	 */
	public function getCopy($id = null)
	{
		return $this->showForm($id, 'copy');
	}

	/**
	 * Blog copy form processing.
	 *
	 * @return Redirect
	 */
	public function postCopy()
	{
		return $this->processForm();
	}

	/**
	 * Blog delete.
	 *
	 * @param  mixed  $id
	 * @return Redirect
	 */
	public function getDelete($id = null)
	{
		try
		{
			// Delete the blog
			API::delete("v1/blog/{$id}");

			// Set the success message
			$notifications = with(new Bag)->add('success', Lang::get('xmgravity/blog::message.success.delete'));
		}
		catch (ApiHttpException $e)
		{
			// Set the error message
			$notifications = with(new Bag)->add('error', Lang::get('xmgravity/blog::message.error.delete'));
		}

		// Redirect to the blog management page
		return Redirect::toAdmin('blog')->with('notifications', $notifications);
	}

	/**
	 * Shows the form.
	 *
	 * @param  mixed   $id
	 * @param  string  $pageSegment
	 * @return mixed
	 */
	protected function showForm($id = null, $pageSegment = null)
	{
		try
		{
			// Set the current active menu
			set_active_menu('admin-xmgravity-blog');

			// Blog fallback
			$blog = null;

			// Do we have a blog identifier?
			if ( ! is_null($id))
			{
				// Get the blog information
				$response = API::get("v1/blog/{$id}");
				$blog  = $response['blog'];
			}

			// Show the page
			return View::make('xmgravity/blog::form', compact('blog', 'pageSegment'));
		}
		catch (ApiHttpException $e)
		{
			// Set the error message
			$notifications = with(new Bag)->add('error', $e->getMessage());

			// Return to the blog management page
			return Redirect::toAdmin('blog')->with('notifications', $notifications);
		}
	}

	/**
	 * Processes the form.
	 *
	 * @param  mixed  $id
	 * @return Redirect
	 */
	protected function processForm($id = null)
	{
		try
		{
			// Are we creating a new blog?
			if (is_null($id))
			{
				// Make the request
				$response  = API::post('v1/blog', Input::all());
				$id        = $response['blog']->id;

				// Prepare the success message
				$success = Lang::get('xmgravity/blog::message.success.create');
			}

			// No, we are updating an existing blog
			else
			{
				// Make the request
				API::put("v1/blog/{$id}", Input::all());

				// Prepare the success message
				$success = Lang::get('xmgravity/blog::message.success.update');
			}

			// Set the success message
			$notifications = with(new Bag)->add('success', $success);

			// Redirect to the blog edit page
			return Redirect::toAdmin("blog/edit/{$id}")->with('notifications', $notifications);
		}
		catch (ApiHttpException $e)
		{
			// Redirect to the appropriate page
			return Redirect::back()->withInput()->withErrors($e->getErrors());
		}
	}

}