<?php
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2013, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return array(

	'name'         => 'Name',
	'name_help'    => 'Type a descriptive name for your content.',
	'slug'         => 'Slug',
	'slug_help'    => 'Slug to find content by.',
	'enabled'      => 'Enabled',
	'enabled_help' => 'Is the content enabled or not?',
	'type'         => 'Storage Type',
	'type_help'    => 'How do you want to store and edit this content?',
	'value'        => 'Content',
	'value_help'   => 'Value of your content. @content call is allowed.',

	'create' => array(
		'legend'  => 'Add Content',
		'summary' => 'Please supply the following information.',
	),

	'update' => array(
		'legend'  => 'Edit Content',
		'summary' => 'Please supply the following information.',
	),

	'copy' => array(
		'legend'  => 'Copy Content',
		'summary' => 'Please supply the following information.',
	),

	// Content types
	'database'   => 'Database',
	'filesystem' => 'Filesystem',

);
