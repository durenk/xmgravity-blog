<?php
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2013, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return array(

	// General messages
	'blog_not_found' => 'Blog [:id] does not exist.',

	// Success messages
	'success' => array(
		'create' => 'Blog was successfully created.',
		'update' => 'Blog was successfully updated.',
		'delete' => 'Blog was successfully deleted.',
	),

	// Error messages
	'error' => array(
		'create' => 'There was an issue creating the blog. Please try again.',
		'update' => 'There was an issue updating the blog. Please try again.',
		'delete' => 'There was an issue deleting the blog. Please try again.',
	),

);
