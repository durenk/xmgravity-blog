@extends('templates/default')

{{-- Page title --}}
@section('title', trans("xmgravity/blog::general.{$pageSegment}.title", array('name' => ! empty($content) ? $content->name : null)))

{{-- Queue Assets --}}
{{ Asset::queue('redactor', 'styles/css/vendor/imperavi/redactor.css', 'style') }}
{{ Asset::queue('redactor', 'js/vendor/imperavi/redactor.js', 'jquery') }}
{{ Asset::queue('slugify', 'js/vendor/platform/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'js/vendor/platform/validate.js', 'jquery') }}
{{ Asset::queue('blog', 'xmgravity/blog::js/blog.js', 'jquery') }}
{{ Asset::queue('redactor-editor', 'xmgravity/blog::js/editor.js', 'redactor') }}

{{-- Partial Assets --}}
@section('assets')
@parent
@stop

{{-- Inline Styles --}}
@section('styles')
@parent
@stop

{{-- Inline Scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('content')
<section id="content">

	<form id="blog-form" class="form-horizontal" action="{{ Request::fullUrl() }}" method="POST" accept-char="UTF-8" autocomplete="off">

		{{-- CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="clearfix">
			<h1 class="pull-left">
				<a class="icon-reply" href="{{ URL::toAdmin('blog') }}"></a> {{ trans("xmgravity/blog::general.{$pageSegment}.title", array('name' => ! empty($blog) ? $blog->name : null)) }}
			</h1>

			<nav class="utilities pull-right">
				<ul>
					@if( ! empty($blog) and $pageSegment != 'copy')
					<li>
						<a class="btn btn-action tip" data-placement="bottom" data-toggle="modal" data-target="#platform-modal-confirm" href="{{ URL::toAdmin("blog/delete/{$blog->id}") }}" title="{{ trans('button.delete') }}"><i class="icon-trash"></i></a>
					</li>
					<li>
						<a class="btn btn-action tip" data-placement="bottom" href="{{ URL::toAdmin("blog/copy/{$blog->id}") }}" title="{{ trans('button.copy') }}"><i class="icon-copy"></i></a>
					</li>
					@endif
					<li>
						<button class="btn btn-action tip" data-placement="bottom" title="{{ trans('button.update') }}" type="submit"><i class="icon-save"></i></button>
					</li>
				</ul>
			</nav>
		</header>

		<hr>

		<section class="content">

			<fieldset>
				<legend>{{ trans("xmgravity/blog::form.{$pageSegment}.legend") }}</legend>

				{{-- Name --}}
				<div class="control-group{{ $errors->first('name', ' error') }}">
					<label class="control-label" for="name">{{ trans('xmgravity/blog::form.name') }}</label>
					<div class="controls">
						<input type="text" name="name" id="name" value="{{{ Input::old('name', ! empty($blog) ? $blog->name : null) }}}" placeholder="{{ trans('xmgravity/blog::form.name') }}" required>
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				{{-- Slug --}}
				<div class="control-group{{ $errors->first('slug', ' error') }}">
					<label class="control-label" for="slug">{{ trans('xmgravity/blog::form.slug') }}</label>
					<div class="controls">
						<input type="text" name="slug" id="slug" value="{{{ Input::old('slug', ! empty($blog) ? $blog->slug : null) }}}" placeholder="{{ trans('xmgravity/blog::form.slug') }}" required>
						{{ $errors->first('slug', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				{{-- Enabled --}}
				<div class="control-group{{ $errors->first('enabled', ' error') }}">
					<label class="control-label" for="enabled">{{ trans('xmgravity/blog::form.enabled') }}</label>
					<div class="controls">
						<select name="enabled" id="enabled" required>
							<option value="1"{{ (Input::old('enabled', ! empty($blog) ? (int) $blog->enabled : 1) === 1 ? ' selected="selected"' : null) }}>{{ trans('general.enabled') }}</option>
							<option value="0"{{ (Input::old('enabled', ! empty($blog) ? (int) $blog->enabled : 1) === 0 ? ' selected="selected"' : null) }}>{{ trans('general.disabled') }}</option>
						</select>
						{{ $errors->first('enabled', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				{{-- Value --}}
				<div class="control-group{{ $errors->first('value', ' error') }}">
					<label class="control-label" for="value">{{ trans('xmgravity/blog::form.value') }}</label>
					<div class="controls">
						<textarea rows="10" name="value" id="value"{{{ Input::old('type', ! empty($blog) ? $blog->type : null) == 'database' ? ' required' : null }}}>{{ Input::old('value', ! empty($blog) ? $blog->value : null) }}</textarea>
						{{ $errors->first('value', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

			</fieldset>

		</section>

		{{-- Form Actions --}}
		<footer>
			<nav class="utilities pull-right">
				<ul>
					@if( ! empty($blog) and $pageSegment != 'copy')
					<li>
						<a class="btn btn-action tip" data-placement="bottom" data-toggle="modal" data-target="#platform-modal-confirm" href="{{ URL::toAdmin("blog/delete/{$blog->id}") }}" title="{{ trans('button.delete') }}"><i class="icon-trash"></i></a>
					</li>
					<li>
						<a class="btn btn-action tip" data-placement="bottom" href="{{ URL::toAdmin("blog/copy/{$blog->id}") }}" title="{{ trans('button.copy') }}"><i class="icon-copy"></i></a>
					</li>
					@endif
					<li>
						<button class="btn btn-action tip" data-placement="bottom" title="{{ trans('button.update') }}" type="submit"><i class="icon-save"></i></button>
					</li>
				</ul>
			</nav>
		</footer>

	</form>

</section>
@stop
