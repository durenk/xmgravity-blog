@extends('templates/default')

{{-- Page title --}}
@section('title')
{{ trans('xmgravity/blog::general.title') }} ::
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('xmgravity-blog', 'xmgravity/blog::css/style.css', 'bootstrap') }}
{{ Asset::queue('xmgravity-blog', 'xmgravity/blog::js/script.js', 'jquery') }}
{{ Asset::queue('tempo', 'js/vendor/tempo/tempo.js', 'jquery') }}
{{ Asset::queue('data-grid', 'js/vendor/cartalyst/data-grid.js', 'tempo') }}

{{-- Partial Assets --}}
@section('assets')
@parent
@stop

{{-- Inline Styles --}}
@section('styles')
@parent
@stop

{{-- Inline Scripts --}}
@section('scripts')
@parent
<script>
jQuery(document).ready(function($) {
	$.datagrid('main', '#grid', '.grid-pagination', '.applied', {
		loader: '.loading',
		type: 'single',
		sort: {
			column: 'created_at',
			direction: 'desc'
		},
		callback: function(obj) {
			$('#total').html(obj.filterCount);

			$('[data-title]').tooltip();
		}
	});
});
</script>
@stop

{{-- Page content --}}
@section('content')
<section id="xmgravity-blog">

	<header class="clearfix">
		<h1 class="pull-left">{{ trans('xmgravity/blog::general.title') }}</h1>

		<nav class="utilities pull-left">
			<ul>
				<li>
					<a class="btn btn-action tip" data-placement="bottom" href="{{ URL::toAdmin('blog/create') }}" data-title="{{ trans('button.create') }}"><i class="icon-plus"></i></a>
				</li>
			</ul>
		</nav>

		<nav class="tertiary-navigation pull-right">
			@widget('platform/menus::nav.show', array(2, 1, 'nav nav-pills', admin_uri()))
		</nav>
	</header>

	<hr>

	<section class="content">

		<div class="grid-functions clearfix">

			<form method="post" action="" accept-charset="utf-8" data-search data-grid="main" class="filters pull-left">
				<div class="styled">
					<select name="column">
						<option value="all">{{ trans('general.all') }}</option>
						<option value="name">{{ trans('xmgravity/blog::table.name') }}</option>
						<option value="slug">{{ trans('xmgravity/blog::table.slug') }}</option>
						<option value="created_at">{{ trans('xmgravity/blog::table.created_at') }}</option>
					</select>
				</div>

				<div class="input-append pull-left">
					<input name="filter" type="text" placeholder="{{ trans('general.search') }}" class="input-large">
					<button class="btn btn-large"><i class="icon-search"></i></button>
				</div>

				<ul class="applied pull-left" data-grid="main">
					<li data-template style="display: none;" class="btn-group">
						<a class="btn btn-large" href="#">
							[? if column == undefined ?]
							[[ valueLabel ]]
							[? else ?]
							[[ valueLabel ]] {{ trans('general.in') }} [[ columnLabel ]]
							[? endif ?]
						</a>
						<a href="#" class="btn btn-large remove-filter"><i class="icon-remove-sign"></i></a>
					</li>
				</ul>
			</form>

			<div class="grid-pagination pull-right" data-grid="main">
				<div data-template style="display: none;">
					<span class="page-meta">[[ pageStart ]] - [[ pageLimit ]] {{ trans('general.of') }} <span id="total"></span></span>
					[? if prevPage !== null ?]
					<a href="#" data-page="[[ prevPage ]]" class="btn">
						<i class="icon-chevron-left"></i>
					</a>
					[? else ?]
					<span class="btn disabled btn-circle">
						<i class="icon-chevron-left"></i>
					</span>
					[? endif ?]

					[? if nextPage !== null ?]
					<a href="#" data-page="[[ nextPage ]]" class="btn">
						<i class="icon-chevron-right"></i>
					</a>
					[? else ?]
					<span class="btn disabled btn-circle">
						<i class="icon-chevron-right"></i>
					</span>
					[? endif ?]
				</div>
			</div>

		</div>

		<div class="grid-wrap clearfix">

			<div class="loading">
				<div class="loading-wrap">
					<div class="cell">
						{{ trans('general.loading') }}
						<br>
						<span class="loader"></span>
					</div>
				</div>
			</div>

			<table id="grid" data-source="{{ URL::toAdmin('blog/grid') }}" data-grid="main" class="table">
				<thead>
					<tr>
						<th data-sort="name" data-grid="main" class="sortable">{{ trans('xmgravity/blog::table.name') }}</th>
						<th data-sort="slug" data-grid="main" class="sortable">{{ trans('xmgravity/blog::table.slug') }}</th>
						<th data-sort="enabled" data-grid="main" class="sortable">{{ trans('xmgravity/blog::table.enabled') }}</th>
						<th data-sort="created_at" data-grid="main" class="span2 sortable">{{ trans('xmgravity/blog::table.created_at') }}</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr data-template style="display: none;">
						<td>[[ name ]]</td>
						<td>[[ slug ]]</td>
						<td>
							[? if enabled == 1 ?]
							{{ trans('general.enabled') }}
							[? else ?]
							{{ trans('general.disabled') }}
							[? endif ?]
						</td>
						<td>[[ created_at | date 'DD MMMM YYYY' ]]</td>
						<td>
							<div class="actions">
								<a class="btn btn-action" data-toggle="modal" data-target="#platform-modal-confirm" href="{{ URL::toAdmin('blog/delete/[[ id ]]') }}" data-title="{{ trans('button.delete') }}"><i class="icon-trash"></i></a>
								<a class="btn btn-action" href="{{ URL::toAdmin('blog/copy/[[ id ]]') }}" data-title="{{ trans('button.copy') }}"><i class="icon-copy"></i></a>
								<a class="btn btn-action" href="{{ URL::toAdmin('blog/edit/[[ id ]]') }}" data-title="{{ trans('button.edit') }}"><i class="icon-edit"></i></a>
							</div>
						</td>
					</tr>
					<tr data-results-fallback style="display: none;">
						<td colspan="5" class="no-results">
							{{ trans('table.no_results') }}
						</td>
					</tr>
				</tbody>
			</table>

		</div>

	</section>

</section>
@stop

